import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:country_picker/country_picker.dart';
import 'package:dev_test/bloc/auth_bloc.dart';
import 'package:dev_test/bloc/profile_bloc.dart';
import 'package:dev_test/helper/app_colors.dart';
import 'package:dev_test/helper/app_fonts.dart';
import 'package:dev_test/helper/utils.dart';
import 'package:dev_test/helper/validators.dart';
import 'package:dev_test/model/user_model.dart';
import 'package:dev_test/storage/data_store.dart';
import 'package:dev_test/ui/widgets/main_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shimmer/shimmer.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  ProfileBloc _profileBloc = ProfileBloc();
  AuthBloc authBloc = new AuthBloc();
  TextEditingController _userNameController = new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _isLoading = BehaviorSubject<bool>();

  CountryCode? countryInfo;
  TextEditingController _emailController = new TextEditingController();
  final _currentFileImage = BehaviorSubject<File>();
  bool isImageRemoveOnTapIsClicked = false;
  late String userName;
  late String email;
 late File file;
  late String currentUserEmail;

late  String currentUsername;
 late Country currentCountry;
  late int countryId;

  setIsLoading(isLoading) {
    _isLoading.sink.add(isLoading);
  }


  Future getImage(String type) async {
    final PickedFile? image = await ImagePicker().getImage(source: type == "camera" ? ImageSource.camera : ImageSource.gallery);
    _currentFileImage.sink.add(File(image!.path));
    file = File(image.path);
    // _image = File(image.path);
    //print("this is image-----------------------------------------and this ${_currentFileImage.value.path}");
  }

  @override
  void initState() {
    super.initState();
    currentUsername = dataStore.user.data!.me!.name!;
    currentUserEmail = dataStore.user.data!.me!.email!;
    isImageRemoveOnTapIsClicked = false;
    _profileBloc.getMyProfile(
        context: context,
        onData: (v) {
        });
  }

  @override
  void dispose() {
    _userNameController.dispose();
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.white,
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.32,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height * 0.25,
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: AppColors.mainGradient,
                            color: AppColors.grey,
                            borderRadius: BorderRadius.only(
                              bottomLeft: const Radius.circular(20),
                              bottomRight: const Radius.circular(20),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.arrow_back_ios_outlined,
                                color: AppColors.white,
                                size: 18,
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                          Text(
                            "Edit Profile",
                            style: TextStyle(
                              color: AppColors.white,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 14),
                            child: Container(
                              width: 25,
                              height: 0,
                            ),
                          ),
                        ],
                      ),
                      StreamBuilder<Me>(
                          stream: _profileBloc.myProfileDataStream,
                          builder: (context, updateMyProfileSnapshot) {
                            if (updateMyProfileSnapshot.hasData) {
                              userName = updateMyProfileSnapshot.data!.name!;
                              email = updateMyProfileSnapshot.data!.email!;
                              return _buildHeader(
                                  updateMyProfileSnapshot: updateMyProfileSnapshot, currentImageSnapshot: _currentFileImage);
                            } else {
                              return _buildHeaderLoading();
                            }
                          }),
                    ],
                  ),
                ),
                StreamBuilder<Me>(
                    stream: _profileBloc.myProfileDataStream,
                    builder: (context, getSnapshotProfile) {
                      if (getSnapshotProfile.hasData) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 32),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 16,
                              ),
                              Stack(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 32,
                                      ),
                                      Text(
                                        "Username",
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 32, right: 32),
                                    child: TextFormField(
                                        controller: _userNameController,
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(top: 32, bottom: 12),
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                color: AppColors.grey,
                                              )),
                                          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppColors.grey)),
                                          hintText: getSnapshotProfile.data!.name != null ? getSnapshotProfile.data!.name : 'Enter Your name',
                                          hintStyle: TextStyle(fontSize: AppFonts.getMediumFontSize(), color: AppColors.f_grey),
                                        )),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Stack(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 32,
                                      ),
                                      Text(
                                        "Email Address",
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 32, left: 32),
                                    child: TextFormField(
                                        validator: (value) {
                                          return emailValidator(email: value,);
                                        },
                                        style: TextStyle(color: AppColors.f_grey),
                                        controller: _emailController,
                                        keyboardType: TextInputType.emailAddress,
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(top: 36, bottom: 12),
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                color: AppColors.grey,
                                              )),
                                          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppColors.grey)),
                                          hintText:
                                          getSnapshotProfile.data!.email ?? "Enter Your email",
                                          // ? getSnapshotProfile.data.email
                                          // : AppLocalizations.of(context).trans('enter_your_email'),
                                          hintStyle: TextStyle(
                                            fontSize: AppFonts.getMediumFontSize(),
                                            color: AppColors.f_grey.withOpacity(0.9),
                                          ),
                                        )),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 16,
                              ),
                            ],
                          ),
                        );
                      } else {
                        return CircularProgressIndicator(
                          color: AppColors.purple,
                        );
                      }
                    }),
                SizedBox(
                  height: 32,
                ),
                StreamBuilder<bool>(
                    stream: _isLoading.stream,
                    initialData: false,
                    builder: (context, snapshot) {
                      return MainButton(
                        text: "Update Profile",
                        isLoading: snapshot.data!,
                        onTap: () async {
                          File? selectedFile;
                          // if (!isImageRemoveOnTapIsClicked) {
                          //   selectedFile = await Utils.compressAndGetFile(File(_currentFileImage.value.path));
                          // }
                          setIsLoading(true);
                          print(_emailController.value.text);
                          if (_emailController.value.text.isNotEmpty && currentUserEmail == null) {
                            _formKey.currentState!.save();

                            if(!_formKey.currentState!.validate()){
                              setIsLoading(false);
                              return;
                            }
                          }else{ _profileBloc.editProfile(
                              name: _userNameController.value.text.isEmpty ? userName : _userNameController.value.text,
                              email: _emailController.value.text.isEmpty? null :_emailController.value.text,
                              onData: (v) {
                                setIsLoading(false);
                                Utils.instance.showToast("Profile Updated");
                                _profileBloc.getMyProfile(context: context, onData: (v) {});
                              },
                              onError: (e) {
                                setIsLoading(false);
                                //print('dasdsad');
                              });}

                          // else {
                          //   callEdit();
                          // }
                        },
                      );
                    }),
                SizedBox(
                  height: 32,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildHeader({AsyncSnapshot<Me>? updateMyProfileSnapshot, BehaviorSubject<File>? currentImageSnapshot}) {
    return Positioned(
      bottom: 0,
      left: 32,
      right:  32,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          StreamBuilder<File>(
              stream: currentImageSnapshot!.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Container(
                        height: MediaQuery.of(context).size.width / 2.9,
                        width: MediaQuery.of(context).size.width / 2.8,
                        child: Image.file(
                          snapshot.data!,
                          fit: BoxFit.cover,
                        )),
                  );
                } else {
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Container(
                        height: MediaQuery.of(context).size.width / 2.9,
                        width: MediaQuery.of(context).size.width / 2.8,
                        child: updateMyProfileSnapshot!.data!.image != null && !isImageRemoveOnTapIsClicked
                            ? CachedNetworkImage(
                          imageUrl: updateMyProfileSnapshot.data!.image!,
                          fit: BoxFit.cover,
                        )
                            : Image.asset(
                          'assets/images/placeholder-final-user.png',
                          fit: BoxFit.cover,
                        )),
                  );
                }
              }),
          SizedBox(
            width: 15,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 60),
            child: Text(
              updateMyProfileSnapshot?.data?.name ?? "",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHeaderLoading() {
    return Positioned(
      bottom: 0,
      left: 32 ,
      right:  32,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: Shimmer.fromColors(
              baseColor: AppColors.grey.withOpacity(0.8),
              highlightColor: AppColors.grey.withOpacity(0.2),
              child: Container(
                height: MediaQuery.of(context).size.width / 2.9,
                width: MediaQuery.of(context).size.width / 2.8,
                color: Colors.grey,
              ),
            ),
          ),
          SizedBox(
            width: 15,
          ),
          Column(
            children: [
              Padding(
                  padding: const EdgeInsets.only(top: 55),
                  child: Shimmer.fromColors(
                      baseColor: AppColors.grey.withOpacity(0.8),
                      highlightColor: AppColors.grey.withOpacity(0.2),
                      child: Container(
                        height: 18,
                        width: 70,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.grey,
                        ),
                      ))),
            ],
          ),
        ],
      ),
    );
  }

  void _onChangeImageWidgetPressed() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        context: context,
        builder: (context) {
          return Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: const Radius.circular(8), topRight: const Radius.circular(8))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                    leading: Icon(Icons.camera_alt_outlined),
                    title: Text("Open Camera"),
                    onTap: () {
                      Navigator.pop(context);
                      getImage("camera");
                    }),
                ListTile(
                    leading: Icon(Icons.camera),
                    title: Text("Oper Gallery"),
                    onTap: () {
                      Navigator.pop(context);
                      getImage("gallery");
                    }),
                // ListTile(
                //     leading: Icon(Icons.restore_from_trash_outlined),
                //     title: Text("Remove"),
                //     onTap: () {
                //       Navigator.pop(context);
                //       // isPhotoRemove.sink.add(true);
                //       // isImageRemoveOnTapIsClicked = true;
                //     }),
              ],
            ),
          );
        });
  }
}
