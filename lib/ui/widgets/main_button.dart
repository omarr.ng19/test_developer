import 'package:dev_test/helper/app_colors.dart';
import 'package:dev_test/helper/app_fonts.dart';
import 'package:flutter/material.dart';

class MainButton extends StatefulWidget {
  final String? text;

  final Color? color;

  final Icon? icon;

  final Gradient? gradient;

  final bool isLoading;

  final VoidCallback onTap;

  MainButton(
      {this.text,
      this.color,
      required this.onTap,
      this.isLoading = false,
      this.gradient,
      this.icon});

  @override
  _MainButtonState createState() => _MainButtonState();
}

class _MainButtonState extends State<MainButton> {
  @override
  Widget build(BuildContext context) {
    if (widget.isLoading) {
      return CircularProgressIndicator(
        color: AppColors.purple,
      );
    } else {
      return Container(
        height: MediaQuery.of(context).orientation == Orientation.landscape
            ? MediaQuery.of(context).size.width * 0.07
            : MediaQuery.of(context).size.height * 0.07,
        width: MediaQuery.of(context).orientation == Orientation.landscape
            ? MediaQuery.of(context).size.height / 1.4
            : MediaQuery.of(context).size.width / 1.4,
        child: RaisedButton(
          onPressed: widget.onTap,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          padding: EdgeInsets.all(0.0),
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [AppColors.dark_purple, AppColors.purple],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                ),
                borderRadius: BorderRadius.circular(8.0)),
            alignment: Alignment.center,
            child: Text(
              widget.text!,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: AppColors.min_white,
                fontSize: AppFonts.getSmallFontSize(),
              ),
            ),
          ),
        ),
      );
    }
  }
}
