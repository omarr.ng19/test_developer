import 'package:dev_test/helper/app_colors.dart';
import 'package:dev_test/helper/app_fonts.dart';
import 'package:flutter/material.dart';

class MainTextField extends StatefulWidget {
  final String? text;
  final TextInputType? inputType;
  final TextEditingController? controller;
  final Function? onTap;
  final FormFieldValidator? validator;
  final Color? color;
  final IconData? icon;
  final bool enable;
  final bool obscureText;
  final FocusNode? focusNode;
  final Widget? isCodePicker;
  final Function onChangeText;
  final String? hintText;

  const MainTextField(
      {this.text,
      this.controller,
      this.onTap,
      this.validator,
      this.color = AppColors.grey,
      this.icon,
      this.inputType,
      this.enable = true,
      this.obscureText = false,
      this.focusNode,
      this.isCodePicker,
      required this.onChangeText,
      this.hintText});

  @override
  _MainTextFieldState createState() => _MainTextFieldState();
}

class _MainTextFieldState extends State<MainTextField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
        style: const TextStyle(color: AppColors.grey),
        autofocus: false,
        focusNode: widget.focusNode,
        obscureText: widget.obscureText,
        enabled: widget.enable,
        controller: widget.controller,
        validator: widget.validator,
        keyboardType: widget.inputType,
        onChanged: widget.onChangeText(),
        decoration: InputDecoration(
          enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(
            color: AppColors.grey,
          )),
          focusedBorder: const UnderlineInputBorder(borderSide: const BorderSide(color: AppColors.grey)),
          labelText: widget.text,
          hintText: widget.hintText,
          hintStyle: TextStyle(
            color: AppColors.f_grey.withOpacity(0.5),
            fontSize: AppFonts.getMediumFontSize(),
          ),
          labelStyle: TextStyle(
            fontSize: AppFonts.getMediumFontSize(),
            color: AppColors.f_grey.withOpacity(0.5),
          ),
          suffixIcon: Padding(
            padding: const EdgeInsets.only(
              top: 16,
              left: 16,
            ),
            child: Icon(
              widget.icon,
              size: 25,
              color: AppColors.grey.withOpacity(0.6),
            ),
          ),
        ));
  }
}
