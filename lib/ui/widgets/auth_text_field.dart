import 'package:dev_test/helper/app_colors.dart';
import 'package:dev_test/helper/app_fonts.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class AuthTextField extends StatefulWidget {
  final String? hint;
  final TextInputType? inputType;
  final TextEditingController? controller;
  final Function onTap;
  final FormFieldValidator? validator;
  final Color? color;
  final IconData? icon;
  final bool? obscureText;
  final withObscureSuffix;

  final Function(String)? onChange;

  AuthTextField(
      {@required this.hint,
      this.withObscureSuffix = true,
      this.onChange,
      this.inputType,
      this.controller,
     required this.onTap,
      this.validator,
      this.color,
      this.icon,
      this.obscureText});

  @override
  _AuthTextFieldState createState() => _AuthTextFieldState();
}

class _AuthTextFieldState extends State<AuthTextField> {
  TextEditingController textEditingController = new TextEditingController();
  final _obscureText = BehaviorSubject<bool>();

  @override
  void initState() {
    super.initState();
    _obscureText.sink.add(true);
  }

  @override
  void dispose() {
    super.dispose();
    _obscureText.close();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap(),
      child: StreamBuilder<bool>(
          stream: _obscureText.stream,
          builder: (context, obscureTextSnapshot) {
            if (obscureTextSnapshot.hasData && obscureTextSnapshot.data != null) {
              return Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      style: TextStyle(color: AppColors.grey),
                      controller: widget.controller,
                      validator: widget.validator,
                      keyboardType: widget.inputType,
                      decoration: InputDecoration(
                        suffixIcon: InkWell(
                          onTap: () {
                            _obscureText.sink.add(!_obscureText.value);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(top: 16, left: 16),
                            child: Icon(
                              obscureTextSnapshot.data! ? Icons.visibility_off_outlined : Icons.visibility_outlined,
                              color: AppColors.grey,
                            ),
                          ),
                        ),
                        enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppColors.grey)),
                        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppColors.grey)),
                        labelText: widget.hint,
                        labelStyle: TextStyle(fontSize: AppFonts.getMediumFontSize(), color: AppColors.f_grey.withOpacity(0.5)),
                      ),
                      onChanged: widget.onChange,
                      obscureText: widget.withObscureSuffix && obscureTextSnapshot.data == true ? true : false,
                    ),
                  ),
                ],
              );
            } else {
              return Container();
            }
          }),
    );
  }
}
