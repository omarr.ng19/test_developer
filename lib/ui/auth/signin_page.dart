import 'package:country_code_picker/country_code_picker.dart';
import 'package:dev_test/bloc/auth_bloc.dart';
import 'package:dev_test/helper/app_colors.dart';
import 'package:dev_test/helper/app_fonts.dart';
import 'package:dev_test/helper/utils.dart';
import 'package:dev_test/helper/validators.dart';
import 'package:dev_test/ui/myProfile.dart';
import 'package:dev_test/ui/widgets/auth_text_field.dart';
import 'package:dev_test/ui/widgets/custom_text_fields.dart';
import 'package:dev_test/ui/widgets/main_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  AuthBloc authBloc = new AuthBloc();
  final TextEditingController _credentialController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _isLoading = BehaviorSubject<bool>();

  setIsLoading(isLoading) {
    _isLoading.sink.add(isLoading);
  }




  late CountryCode countryInfo;

  final BehaviorSubject<AuthCredentialType> _authCredentialType = BehaviorSubject.seeded(AuthCredentialType.EMAIL);

  setAuthCredentialType(AuthCredentialType type) {
    _authCredentialType.sink.add(type);
  }

  _onLoginPress() {
    if (_credentialController.text.trim().toLowerCase().isNotEmpty && _passwordController.text.trim().isNotEmpty) {
      String countryCode;
      if (_authCredentialType.value == AuthCredentialType.PHONE) {
        countryCode = countryInfo.dialCode!.substring(1);
      }
      setIsLoading(true);
      print("22222222");
      authBloc.login(
          email: _credentialController.value.text.trim(),
          password: _passwordController.text.trim(),
          context: context,
          onData: (data) {
            print("33333");
            setIsLoading(false);
            Navigator.pushAndRemoveUntil(
                context, MaterialPageRoute(builder: (BuildContext context) => MyProfilePage()), (Route<dynamic> route) => false);},
          onError: (e) {
            // print(e.code);
             setIsLoading(false);
            // if(e.code == 403){
            //   FocusScope.of(context).unfocus();
            //   return;
            // }
            Utils.instance.showToast("Invalid login");
          });
    }
  }

  @override
  void dispose() {
    super.dispose();
    _authCredentialType.close();
    _isLoading.close();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.white,
        body: Form(
          key: _formKey,
          child: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.45,
                width: MediaQuery.of(context).size.width,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: AppColors.mainGradient,
                    color: AppColors.grey,
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8, top: 8),
                child: IconButton(
                    icon: const Icon(
                      Icons.arrow_back_ios_outlined,
                      color: AppColors.white,
                      size: 18,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(
                    height: 80,
                  ),
                  Column(
                    children: [
                      Row(
                        children: [
                          const SizedBox(
                            width: 32,
                          ),
                          const Text(
                            "Welcome",
                            style: TextStyle(fontSize: 30, color: AppColors.white, height: 1.5),
                          )
                        ],
                      )
                    ],
                  ),
                  Expanded(
                      child: CustomScrollView(
                    slivers: [
                      SliverList(
                        delegate: SliverChildListDelegate([
                          Padding(
                            padding: const EdgeInsets.only(left: 32, top: 24, right: 32),
                            child: Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      color: AppColors.white,
                                      borderRadius: BorderRadius.circular(28),
                                      boxShadow: [const BoxShadow(color: AppColors.f_grey, blurRadius: 4, offset: const Offset(0, 2))]),
                                  child: Column(
                                    children: [
                                      const Padding(
                                        padding: EdgeInsets.only(top: 16.0,),
                                        child: Text(
                                          "Sign in to continue",
                                          style: TextStyle(
                                            color: AppColors.dark_black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 32,),
                                      StreamBuilder<AuthCredentialType>(
                                          stream: _authCredentialType.stream,
                                          builder: (context, colorSnapshot) {
                                            if (colorSnapshot.hasData) {
                                              return Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  InkWell(
                                                      onTap: () {
                                                        _credentialController.clear();
                                                        setAuthCredentialType(AuthCredentialType.EMAIL);
                                                      },
                                                      child: Text(
                                                       "Email",
                                                        style: TextStyle(
                                                            fontSize: 16,
                                                            color: AppColors.grey,
                                                            fontWeight: colorSnapshot.data == AuthCredentialType.EMAIL
                                                                ? FontWeight.bold
                                                                : FontWeight.normal),
                                                      )),
                                                  const SizedBox(
                                                    width: 14,
                                                  ),
                                                  Container(
                                                    height: 17.11,
                                                    width: 1,
                                                    decoration: const BoxDecoration(color: Colors.grey),
                                                  ),
                                                  const SizedBox(
                                                    width: 14,
                                                  ),
                                                  InkWell(
                                                      onTap: () {
                                                        _credentialController.clear();
                                                        setAuthCredentialType(AuthCredentialType.PHONE);
                                                      },
                                                      child: Text(
                                                        "Phone",
                                                        style: TextStyle(
                                                            fontSize: 16,
                                                            color: AppColors.grey,
                                                            fontWeight: colorSnapshot.data == AuthCredentialType.PHONE
                                                                ? FontWeight.bold
                                                                : FontWeight.normal),
                                                      )),
                                                ],
                                              );
                                            }
                                            return Container();
                                          }),
                                      StreamBuilder<AuthCredentialType>(
                                          stream: _authCredentialType.stream,
                                          builder: (context, snapshot) {
                                            if (snapshot.hasData) {
                                              if (snapshot.data == AuthCredentialType.EMAIL) {
                                                return Padding(
                                                  padding: const EdgeInsets.only(
                                                    left: 32,
                                                    right: 32,
                                                  ),
                                                  child: MainTextField(
                                                      text:"Email address",
                                                      icon: Icons.email,
                                                      controller: _credentialController,
                                                      validator: (value) {
                                                        return emailValidator(email: value);
                                                      },
                                                      inputType: TextInputType.emailAddress, onChangeText: (){},),
                                                );
                                              } else {
                                                return Padding(
                                                  padding: const EdgeInsets.only(
                                                    left: 32,
                                                    right: 32,
                                                  ),
                                                  child: TextFormField(
                                                      keyboardType: TextInputType.number,
                                                      validator: (value) {
                                                        return phoneValidator(phone: value,);
                                                      },
                                                      onChanged: (value) {
                                                        if (value == "0") {
                                                          Utils.instance.showToast("Please enter the number without zeros");
                                                          _credentialController.text = "";
                                                          _credentialController.selection = TextSelection.fromPosition(
                                                            TextPosition(
                                                              offset: _credentialController.text.length,
                                                            ),
                                                          );
                                                        }
                                                      },
                                                      controller: _credentialController,
                                                      style: const TextStyle(color: AppColors.grey),
                                                      decoration: InputDecoration(
                                                        contentPadding: const EdgeInsets.only(top: 26, left: 24),
                                                        enabledBorder: const UnderlineInputBorder(
                                                            borderSide: const BorderSide(
                                                          color: AppColors.grey,
                                                        )),
                                                        focusedBorder: const UnderlineInputBorder(borderSide: BorderSide(color: AppColors.grey)),
                                                        hintText: 'xxx-xxx-xx',
                                                        hintStyle: TextStyle(
                                                          color: AppColors.f_grey.withOpacity(0.5),
                                                        ),
                                                        labelStyle: TextStyle(
                                                          fontSize: AppFonts.getMediumFontSize(),
                                                          color: AppColors.grey,
                                                        ),
                                                        prefixIcon: Padding(
                                                          padding: const EdgeInsets.only(
                                                            top: 16,
                                                          ),
                                                          child: isoCodePicker(onChangeCountryInfo: (code) {
                                                            countryInfo = code;
                                                          }, onInitCountryInfo: (code) {
                                                            countryInfo = code;
                                                          }),
                                                        ),
                                                      )),
                                                );
                                              }
                                            }
                                            return Container();
                                          }),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 32, right: 32, top: 8),
                                        child: AuthTextField(
                                          hint: "Password",
                                          withObscureSuffix: true,
                                          inputType: TextInputType.text,
                                          controller: _passwordController,
                                          validator: (value) {
                                            return passwordValidator(password: value);
                                          }, onTap: (){},
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 30,
                                      ),
                                      StreamBuilder<bool>(
                                          stream: _isLoading.stream,
                                          initialData: false,
                                          builder: (context, snapshot) {
                                            return MainButton(
                                              text: "login",
                                              isLoading: snapshot.data!,
                                              onTap: () {
                                                if (_formKey.currentState!.validate()) {
                                                  _formKey.currentState!.save();
                                                  print("1111111111");
                                                  _onLoginPress();
                                                }
                                              },
                                            );
                                          }),
                                      const SizedBox(
                                        height: 16,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 24,
                                ),
                              ],
                            ),
                          ),
                        ]),
                      ),
                    ],
                  ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget isoCodePicker({required Function onChangeCountryInfo, required Function onInitCountryInfo}) {
    return CountryCodePicker(
      backgroundColor: AppColors.white,
      showFlagDialog: true,
      textStyle: const TextStyle(height: 1.0, color: AppColors.grey),
      favorite: ['SA', 'KW', 'AE', 'BH', 'QA', 'OM'],
      showFlag: false,
      boxDecoration: const BoxDecoration(),
      padding: const EdgeInsets.only(right: 18),
      initialSelection: 'AE',
      dialogTextStyle: const TextStyle(color: AppColors.black),
      searchStyle: const TextStyle(color: AppColors.black),
      onInit: (code) {
        onInitCountryInfo(code);
        countryInfo = code!;
      },
      onChanged: (code) {
        onChangeCountryInfo(code);
      },
    );
  }
}
enum AuthCredentialType { EMAIL, PHONE }
