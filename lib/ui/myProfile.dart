import 'package:cached_network_image/cached_network_image.dart';
import 'package:dev_test/bloc/auth_bloc.dart';
import 'package:dev_test/bloc/profile_bloc.dart';
import 'package:dev_test/helper/app_colors.dart';
import 'package:dev_test/helper/utils.dart';
import 'package:dev_test/model/user_model.dart';
import 'package:dev_test/storage/data_store.dart';
import 'package:dev_test/ui/auth/signin_page.dart';
import 'package:dev_test/ui/edit_profile.dart';
import 'package:dev_test/ui/widgets/main_button.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class MyProfilePage extends StatefulWidget {
  const MyProfilePage({Key? key}) : super(key: key);

  @override
  _MyProfilePageState createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<MyProfilePage> {
  ProfileBloc _profileBloc = ProfileBloc();
  AuthBloc _authBloc = AuthBloc();

  getMe() {
    print("im innnn");
      _profileBloc.getMyProfile(context: context, onData: (v) {});
  }

  @override
  void initState() {
    super.initState();
    getMe();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: AppColors.white,
            body: CustomScrollView(
              slivers: [
                SliverList(
                    delegate: SliverChildListDelegate([
                      SizedBox(
                        height: 32,
                      ),
                     Column(
                        children: [
                          StreamBuilder<Me>(
                              stream: _profileBloc.myProfileDataStream,
                              builder: (context, myProfileSnapshot) {
                                if (myProfileSnapshot.hasData && myProfileSnapshot.data != null) {
                                  return _buildHeader(myProfileSnapshot: myProfileSnapshot);
                                } else {
                                  return _buildHeaderLoading();
                                }
                              }),
                          SizedBox(
                            height: 40,
                          ),
                        ],
                      )
                    ])),
                SliverFillRemaining(
                    fillOverscroll: true,
                    hasScrollBody: false,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MainButton(
                          text: "Edit Profile",
                          onTap: () {  Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => EditProfile(),
                            )).then((value) {
                          getMe();
                        });  },),
                        SizedBox(height: 16,),
                        MainButton(
                          text: "Log Out",
                          onTap: () {
                            _authBloc.logOut(
                                accessToken: dataStore.user.data!.accessToken!,
                                context: context,
                                onData: () {
                                  Navigator.pushReplacement(
                                      context, MaterialPageRoute(builder: (BuildContext context) => SignInPage()));
                                   dataStore.clearStoredData();
                                },
                                onError: (e) {
                                  Utils.instance.showToast("Log out failed");
                                });
                            },),
                      ],
                    ))
              ],
            )));
  }

  Widget _buildHeader({AsyncSnapshot<Me>? myProfileSnapshot}) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: Container(
                  height: MediaQuery.of(context).size.width / 2.9,
                  width: MediaQuery.of(context).size.width / 2.8,
                  child: myProfileSnapshot!.data!.image != null
                      ? CachedNetworkImage(
                    useOldImageOnUrlChange: false,
                    imageUrl: myProfileSnapshot.data!.image,
                    fit: BoxFit.cover,
                  )
                      : Image.asset(
                    'assets/images/placeholder-final-user.png',
                    fit: BoxFit.cover,
                  )),
            ),
            SizedBox(
              width: 20,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  myProfileSnapshot.data!.name!,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildHeaderLoading() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: Shimmer.fromColors(
            baseColor: Colors.grey.withOpacity(0.8),
            highlightColor: Colors.grey.withOpacity(0.1),
            child: Container(
              height: MediaQuery.of(context).size.width / 2.9,
              width: MediaQuery.of(context).size.width / 2.8,
              color: Colors.grey.withOpacity(0.8),
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Shimmer.fromColors(
                baseColor: Colors.grey.withOpacity(0.8),
                highlightColor: AppColors.grey.withOpacity(0.2),
                child: Container(
                  height: 30,
                  width: 55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.grey.withOpacity(0.1),
                  ),
                )),
            SizedBox(
              height: 10,
            ),
            Shimmer.fromColors(
                baseColor: Colors.grey.withOpacity(0.8),
                highlightColor: Colors.grey.withOpacity(0.1),
                child: Container(
                  height: 18,
                  width: 65,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.grey.withOpacity(0.8),
                  ),
                )),
          ],
        ),
      ],
    );
  }
}
