import 'package:dev_test/helper/utils.dart';
import 'package:dev_test/model/general_model.dart';
import 'package:dev_test/model/user_model.dart';
import 'package:dev_test/network_service/api_provider.dart';
import 'package:dev_test/storage/data_store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc {

  login({@required password, email, onData, onError,context}) {
    print("444444");
    print(password);
    print(email);
    print(context);

    apiProvider.login(email: email, password: password,context: context).then((value) {
      // print("134567");
      // print(value.toJson());
      dataStore.setUser(value).then((val) {
        onData(value);
      });

    }).catchError((e) {
      print("0000000");
      print(e);
      // print(e.statusCode);
      onError(e);
      // print("${e.result.message}");
      // onError(e);
      // print(e);
      // Utils.instance.showToast("${e.result.message}");
    });
  }
  // refreshToken({@required String? accessToken, Function? onData, context, onError}) {
  //   apiProvider.logOut(accessToken: accessToken, context: context).then((value) {
  //     // _logOutControler.sink.add(value);
  //     onData!(value);
  //   }).catchError((e) {
  //     print("&&&&&&&&&&&&&");
  //     print(e);
  //     onError(e);
  //   });
  // }

  logOut({@required String? accessToken, Function? onData, context, onError}) {
    apiProvider.logOut(accessToken: accessToken, context: context).then((value) {
      // _logOutControler.sink.add(value);
      onData!();
    }).catchError((e) {
      print("&&&&&&&&&&&&&");
      print(e);
      onError(e);
    });
  }
}