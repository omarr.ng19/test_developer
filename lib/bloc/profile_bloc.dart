import 'package:dev_test/helper/utils.dart';
import 'package:dev_test/model/profile_model.dart';
import 'package:dev_test/model/user_model.dart';
import 'package:dev_test/network_service/api_provider.dart';
import 'package:dev_test/storage/data_store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ProfileBloc {

  var _myprofileData = BehaviorSubject<Me>();

  get myProfileDataStream => _myprofileData.stream;

  getMyProfile({Function? onData, context}) {
    apiProvider.getMyProfile().then((value) {
      print("nnnnnnn");
      print(value.toJson());
      // print("@@@@@@@@@@");s
      // print(value.toJson());
      _myprofileData.sink.add(value);
      // print(value.data!.me!.toString());
     print("sadsadsadsa");
      onData!(value);

      dataStore.getUser().then((val) {
        UserModel user;
        user=val;
        user.data!.me=value;
        user.data!.accessToken = dataStore.user.data!.accessToken;
        dataStore.setUser(user).then((value) {});
      });

    }).catchError((e) {
      print("Dsadsadsad");
      print("error \n$e");
    });
  }
  editProfile({String? name,String? email, Function? onData, Function? onError,BuildContext? context}) {
    apiProvider.updateProfile(name: name,email: email,context: context).then((value) {
      onData!(value);
    }).catchError((e) {
      onError!(e);
      print("edit profile error  in bloc \n$e");
    });
  }

}