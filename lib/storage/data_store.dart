import 'package:dev_test/model/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataStore {
  static final DataStore _singletonBloc = new DataStore._internal();




  UserModel _userModel = UserModel();

  UserModel get user => _userModel;



  factory DataStore() {
    return _singletonBloc;
  }

  DataStore._internal() {


    getUser().then((onVal) {
      _userModel = onVal;
    });
  }
  Future<bool> setUser(UserModel value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _userModel = value;
    return prefs.setString('User', userModelToJson(value));
  }
  //
  Future<UserModel> getUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var x = prefs.getString('User') ?? '';
    var u = x != '' ? userModelFromJson(x) : UserModel();
    print("xxxxxsdasdasd" + x);
    return u;
  }

  clearStoredData() {
    setUser(UserModel(data: Data()));

  }
}

final dataStore = DataStore();
