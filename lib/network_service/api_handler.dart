import 'dart:io';
import 'package:dev_test/helper/utils.dart';
import 'package:dev_test/model/error_model.dart';
import 'package:dev_test/model/user_model.dart';
import 'package:dev_test/network_service/api_links_container.dart';
import 'package:dev_test/network_service/api_provider.dart';
import 'package:dev_test/storage/data_store.dart';
import 'package:dev_test/ui/auth/signin_page.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

abstract class ApiHandler {
  Map<String, String> headers = {
    'Content-Type': 'application/json',
     'Accept': 'application/json',
    'App-Secret': '*(3%13@Uh@1',
    'Platform': 'web',
    // 'Authorization': dataStore.user.data!.accessToken!,
  };
  // Map<String, String> header = {
  //   'Content-Type': 'application/json',
  //   'Accept': 'application/json',
  //   'App-Secret': '*(3%13@Uh@1',
  //   'Platform': 'web',
  //   'Authorization': dataStore.user.data!.refreshToken!,
  // };

  deleteMultiPartCallApi(
    url,
    body,
  ) async {
    ErrorsModel v;
    //print(url);
    Utils.instance.removeNullMapObjects(body);
    if (dataStore.user.data!.accessToken!= null) {
      //print(" is :Bearer  " + dataStore.user.token);
      headers['Authorization'] = "Bearer " + dataStore.user.data!.accessToken.toString();
    } else {
      headers['Authorization'] = null.toString();
    }
    Dio dio = new Dio();
    var data;

    await dio.delete(url, options: Options(headers: headers), data: body).then((val) {
      //print(val.data);
      data = val.data;
      switch (val.data['Result']['update_status'].toLowerCase()) {
        case "required":
          //print("required");
          //forceUpdate(context);
          throw Exception;
          break;
        case "optional":
          //print("optional");

          break;
        default:
          //print("default");
          break;
      }
    }).catchError((e) {
      //print(e.response.statusCode);
      //print(e.response.data);
      //print(e);

      v = ErrorsModel.fromJson(e.response.data);
      throw v;
    });
    return data;
  }

  getMultiPartCallApi(url) async {
    print(url);
    // if(Utils.instance.convertDateTimeToLocal(dataStore.user.data!.accessExpiresAt!).isBefore(DateTime.now())){
    //   headers['Authorization'] = "Bearer ${dataStore.user.data!.refreshToken!}";
    // }
    // else{
      if (dataStore.user.data!.accessToken!= null) {
        print(" is :Bearer  " + dataStore.user.data!.accessToken!);
        headers['Authorization'] = "Bearer ${dataStore.user.data!.accessToken!}";
        print(headers);
      } else {
        headers['Authorization'] = "";
      }
    // }
    Dio dio = new Dio();
    var data;
    await dio
        .get(url,
            options: Options(
              headers: headers,
            ))
        .then((val) {
      print("______________________ response");
      print(val.data);
      data = val.data;
    }).catchError((e) {
      print(e.response.statusCode);
      print(e.response.data);
      try {} catch (e) {}
    });
    return data;
  }
  getMultiPartCallApii(url, context) async {
    print(url);
    // if(Utils.instance.convertDateTimeToLocal(dataStore.user.data!.accessExpiresAt!).isBefore(DateTime.now())){
    //   headers['Authorization'] = "Bearer ${dataStore.user.data!.refreshToken!}";
    // }
    // else{
      if (dataStore.user.data!.accessToken!= null) {
        print(" is :Bearer  " + dataStore.user.data!.accessToken!);
        headers['Authorization'] = "Bearer ${dataStore.user.data!.refreshToken!}";
        print(headers);
      } else {
        headers['Authorization'] = "";
      }
    // }
    Dio dio = new Dio();
    var data;
    await dio
        .get(url,
        options: Options(
          headers: headers,
        ))
        .then((val) {
      print("______________________ response");
      print(val.data);
      data = val.data;
    }).catchError((e) {
      print(e.response.statusCode);
      print(e.response.data);
      try {} catch (e) {
      }
    });
    return data;
  }

  postMultiPartCallApi(
    url,
    body,
    context,
  ) async {
    print(url);
    print(body);
    print(headers);
    // Utils.instance.removeNullMapObjects(body);
    // if (dataStore.user.data!.accessToken!= null) {
    //   print(" is :Bearer  " + dataStore.user.data!.accessToken!);
    //   headers['Authorization'] = "Bearer ${dataStore.user.data!.accessToken!}";
    //   print(headers);
    // } else {
    //   headers['Authorization'] = "";
    // }
    Dio dio = new Dio();
    var data;
    await dio.post(url, data: body, options: Options(headers: headers)).then((val) {
      print("______________________ response");
      print(val.data);
      data = val.data;
    }).catchError((e) {
      print("______________________ error");
      print(e.response.statusCode);

      print(e.response.data);

      print("!!!!!!!!!!!!!!!!!11");
      print(e);
    });
    return data;
  }

  putMultiPartCallApi(
    url,
    body,
  ) async {
    print(url);
    // Utils.instance.removeNullMapObjects(body);
    if (dataStore.user.data!.accessToken!= null) {
      print("zzzzzzzzzz");
      print(" is :Bearer  " + dataStore.user.data!.accessToken!);
      headers['Authorization'] = "Bearer ${dataStore.user.data!.accessToken!}";
    } else {
      headers['Authorization'] = "";
    }
    //print(body);
    Dio dio = new Dio();
    var data;
    try {
      await dio.put(url, data: body, options: Options(headers: headers)).then((val) {
        print("______________________ response");
        print(val.data);
        data = val.data;
      }).catchError((e) {
        print("______________________ error");
        print(e.response.statusCode);
        print(e.response.data);

        print(e);

      });
    } catch (e) {
      print(e);
    }

    return data;
  }

}
