import 'package:dev_test/helper/app_constant.dart';

class ApiLinksContainer{

  //==========auth========
  static const String loginURL = '${AppConstant.apiBaseUrl}auth/admin-login';
  static const String updateProfileURL = '${AppConstant.apiBaseUrl}auth/update';
  static const String logOutURL = '${AppConstant.apiBaseUrl}auth/logout';
  static const String refreshURL = '${AppConstant.apiBaseUrl}auth/refresh';


  //=======profile==========
  static const String getProfileURL = '${AppConstant.apiBaseUrl}me';

}