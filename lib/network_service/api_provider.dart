import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dev_test/helper/utils.dart';
import 'package:dev_test/model/user_model.dart';
import 'package:dev_test/storage/data_store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'api_handler.dart';
import 'api_links_container.dart';

class ApiProvider with ApiHandler {


  Future<Me> getMyProfile() async {
    await refreshToken(accessToken: dataStore.user.data!.accessToken!);
    var response;
    var data = await getMultiPartCallApi(ApiLinksContainer.getProfileURL);
    //print("5555555");
    //print(data);
    try {
      response = Me.fromJson(data['data']);
    } catch (e) {
      //print("get profile error \n $e");
      throw data;
    }
    //print("nvnvnvnv");
    //print(response.toJson());
    return response;
  }



  Future <UserModel> login(
      {String? email, @required String? password, @required BuildContext? context}) async {
    // if(dataStore.user.data!.accessExpiresAt!.isAfter(DateTime.now())){
    //   refreshToken(accessToken: dataStore.user.data!.accessToken!,);
    //   throw Error;
    // }
    // // else
    //    {
    //print("xxxxx");
    Map body = {"email": email, "password": password};
    var data = await postMultiPartCallApi(
        ApiLinksContainer.loginURL, body, context);
    try {
      //print("^^^^mmmmm");
      return UserModel.fromJson(data);
    } catch (e) {
      //print("&&&&&&&&&&&&&&&&7");
      //print(e);
      throw data;
    }
    // }

  }

  Future<Me> updateProfile(
      {String? name, String? email, @required BuildContext? context}) async {
    await refreshToken(accessToken: dataStore.user.data!.accessToken!);
    var response;
    Map body = new Map();
    body["name"] = name;
    //print("Dasdsad");
    //print(email);
    if (email != null) {
      body["email"] = email;
    }
    var data = await putMultiPartCallApi(
      ApiLinksContainer.updateProfileURL, body,);
    try {
        response = Me.fromJson(data);
    } catch (e) {
      //print("&&&&&&&&&&&&&&&&7");
      //print(e);
      throw data;
    }
    return response;
  }


  logOut(
      {@required String? accessToken, @required BuildContext? context}) async {
    await refreshToken(accessToken: dataStore.user.data!.accessToken!);
    String link = "";
    link = ApiLinksContainer.logOutURL + "?access_token=$accessToken";
    var data = await getMultiPartCallApii(link, {});
    try {
      // //print(GeneralModel.fromJson(data).toJson());
      // //print("dasdssxzcxzc");
      // return GeneralModel.fromJson(data);
    } catch (e) {
      //print("EEEEEEEE");
      //print("logougt in api provider \n\n $e");

      throw data;
    }
  }

  Future<bool> refreshToken(
      {@required String? accessToken, Function? onData}) async {
    if (Utils.instance.convertDateTimeToLocal(
        dataStore.user.data!.accessExpiresAt!).isBefore(DateTime.now())) {
      //print("nnnnnnn");
      String link = "";
      link = ApiLinksContainer.refreshURL + "?access_token=$accessToken";
      var data = await getMultiPartCallApii(link, {});
      var responsse =  UserModel.fromJson(data);
      //print(data);

      await dataStore.setUser(responsse);
     return true;
    } else {
      return false;
    }
  }
}

final apiProvider = ApiProvider();
