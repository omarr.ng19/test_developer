// To parse this JSON data, do
//
//     final profileModel = profileModelFromJson(jsonString);

import 'dart:convert';

ProfileModel profileModelFromJson(String str) => ProfileModel.fromJson(json.decode(str));

String profileModelToJson(ProfileModel data) => json.encode(data.toJson());

class ProfileModel {
  ProfileModel({
    this.data,
    this.message,
    this.statusCode,
  });

  ProfileItem? data;
  String? message;
  int? statusCode;

  factory ProfileModel.fromJson(Map<String, dynamic> json) => ProfileModel(
    data: json["data"] == null ? null : ProfileItem.fromJson(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data!.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class ProfileItem {
  ProfileItem({
    this.id,
    this.email,
    this.hasVerifiedEmail,
    this.phone,
    this.hasVerifiedPhone,
    this.name,
    this.image,
    this.permissions,
    this.addresses,
    this.cards,
  });

  int? id;
  String? email;
  bool? hasVerifiedEmail;
  String? phone;
  bool? hasVerifiedPhone;
  String? name;
  dynamic image;
  List<Permission>? permissions;
  dynamic addresses;
  dynamic cards;

  factory ProfileItem.fromJson(Map<String, dynamic> json) => ProfileItem(
    id: json["id"] == null ? null : json["id"],
    email: json["email"] == null ? null : json["email"],
    hasVerifiedEmail: json["has_verified_email"] == null ? null : json["has_verified_email"],
    phone: json["phone"] == null ? null : json["phone"],
    hasVerifiedPhone: json["has_verified_phone"] == null ? null : json["has_verified_phone"],
    name: json["name"] == null ? null : json["name"],
    image: json["image"],
    permissions: json["permissions"] == null ? null : List<Permission>.from(json["permissions"].map((x) => Permission.fromJson(x))),
    addresses: json["addresses"],
    cards: json["cards"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "email": email == null ? null : email,
    "has_verified_email": hasVerifiedEmail == null ? null : hasVerifiedEmail,
    "phone": phone == null ? null : phone,
    "has_verified_phone": hasVerifiedPhone == null ? null : hasVerifiedPhone,
    "name": name == null ? null : name,
    "image": image,
    "permissions": permissions == null ? null : List<dynamic>.from(permissions!.map((x) => x.toJson())),
    "addresses": addresses,
    "cards": cards,
  };
}

class Permission {
  Permission({
    this.id,
    this.name,
  });

  int? id;
  String? name;

  factory Permission.fromJson(Map<String, dynamic> json) => Permission(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
  };
}
