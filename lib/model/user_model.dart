// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data? data;
  String? message;
  int? statusCode;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data!.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.tokenType,
    this.accessToken,
    this.accessExpiresAt,
    this.refreshToken,
    this.refreshExpiresAt,
    this.me,
  });

  String? tokenType;
  String? accessToken;
  DateTime? accessExpiresAt;
  String? refreshToken;
  DateTime? refreshExpiresAt;
  Me? me;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    tokenType: json["token_type"] == null ? null : json["token_type"],
    accessToken: json["access_token"] == null ? null : json["access_token"],
    accessExpiresAt: json["access_expires_at"] == null ? null : DateTime.parse(json["access_expires_at"]),
    refreshToken: json["refresh_token"] == null ? null : json["refresh_token"],
    refreshExpiresAt: json["refresh_expires_at"] == null ? null : DateTime.parse(json["refresh_expires_at"]),
    me: json["me"] == null ? null : Me.fromJson(json["me"]),
  );

  Map<String, dynamic> toJson() => {
    "token_type": tokenType == null ? null : tokenType,
    "access_token": accessToken == null ? null : accessToken,
    "access_expires_at": accessExpiresAt == null ? null : accessExpiresAt!.toIso8601String(),
    "refresh_token": refreshToken == null ? null : refreshToken,
    "refresh_expires_at": refreshExpiresAt == null ? null : refreshExpiresAt!.toIso8601String(),
    "me": me == null ? null : me!.toJson(),
  };
}

class Me {
  Me({
    this.id,
    this.email,
    this.hasVerifiedEmail,
    this.phone,
    this.hasVerifiedPhone,
    this.name,
    this.image,
    this.permissions,
    this.addresses,
    this.cards,
  });

  int? id;
  String? email;
  bool? hasVerifiedEmail;
  String? phone;
  bool? hasVerifiedPhone;
  String? name;
  dynamic image;
  List<Permission>? permissions;
  dynamic addresses;
  dynamic cards;

  factory Me.fromJson(Map<String, dynamic> json) => Me(
    id: json["id"] == null ? null : json["id"],
    email: json["email"] == null ? null : json["email"],
    hasVerifiedEmail: json["has_verified_email"] == null ? null : json["has_verified_email"],
    phone: json["phone"] == null ? null : json["phone"],
    hasVerifiedPhone: json["has_verified_phone"] == null ? null : json["has_verified_phone"],
    name: json["name"] == null ? null : json["name"],
    image: json["image"],
    permissions: json["permissions"] == null ? null : List<Permission>.from(json["permissions"].map((x) => Permission.fromJson(x))),
    addresses: json["addresses"],
    cards: json["cards"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "email": email == null ? null : email,
    "has_verified_email": hasVerifiedEmail == null ? null : hasVerifiedEmail,
    "phone": phone == null ? null : phone,
    "has_verified_phone": hasVerifiedPhone == null ? null : hasVerifiedPhone,
    "name": name == null ? null : name,
    "image": image,
    "permissions": permissions == null ? null : List<dynamic>.from(permissions!.map((x) => x.toJson())),
    "addresses": addresses,
    "cards": cards,
  };
}

class Permission {
  Permission({
    this.id,
    this.name,
  });

  int? id;
  String? name;

  factory Permission.fromJson(Map<String, dynamic> json) => Permission(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
  };
}
