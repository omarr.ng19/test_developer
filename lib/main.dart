import 'package:dev_test/helper/app_colors.dart';
import 'package:dev_test/storage/data_store.dart';
import 'package:dev_test/ui/auth/signin_page.dart';
import 'package:dev_test/ui/myProfile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'helper/app_constant.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    AppConstant.context=context;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const InitPage(),
    );
  }
}

class InitPage extends StatelessWidget {
  const InitPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    AppConstant.screenSize = MediaQuery.of(context).size;
    return const SplashScreen();
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    // dataStore.init();
    Future.delayed(const Duration(seconds: 3)).then((value)=>_openOnBoard());
  }
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        backgroundColor: AppColors.white,
        body: Center(
          child: CircleAvatar(
            foregroundImage: AssetImage(
              "assets/images/splash.gif",
            ),
            radius: 100,
          ),
        ));
  }
  _openOnBoard(){
    dataStore.getUser().then((value) {
      if(value.data!.accessToken == null){
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) =>const SignInPage(),));
    }else{
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) =>const MyProfilePage(),));
    }});


  }
}

