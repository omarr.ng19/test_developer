

import 'dart:io';

import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

class Utils {
  factory Utils() {
    return _utils;
  }

  Utils._internal();

  static final Utils _utils = Utils._internal();

  static Utils get instance => _utils;



  bool isNumeric(String result) {
    if (result == null) {
      return false;
    }
    return double.tryParse(result) != null;
  }

  void showToast(String text) {
    Fluttertoast.showToast(
      msg: (text),
    );
  }
  removeNullMapObjects(Map map) {
    map.removeWhere((key, value) => key == null || value == null);
  }


  static Future<File?> compressAndGetFile(
      File file,
      ) async {
    final tempDir = await path_provider.getTemporaryDirectory();
    String tempTitle = DateFormat('kk:mm:ss_EEE-d-MMM').format(DateTime.now());

    return await FlutterImageCompress.compressAndGetFile(
      file.absolute.path,
      tempDir.absolute.path + "/$tempTitle.jpg",
      quality: 25,
    );
  }

  DateTime convertDateTimeToLocal(DateTime date) {
    return DateFormat("yyyy-MM-dd HH:mm:ss").parse(date.toString(), true).toLocal();
  }





}
